%if 0%{?fedora} > 12
%global with_python3 1
%else
%{!?__python2: %global __python2 /usr/bin/python2}
%{!?python2_sitelib: %global python2_sitelib %(%{__python2} -c "from distutils.sysconfig import get_python_lib; print (get_python_lib())")}
%endif

%global srcname future

Name:		python-%{srcname}
Version:	0.15.2
Release:	1%{?dist}
Summary:	Clean single-source support for Python 2/3

Group:		Development/Languages
License:	MIT
URL:		http://python-future.org/
Source0:	https://pypi.python.org/packages/source/f/future/future-%{version}.tar.gz
# https://github.com/PythonCharmers/python-future/issues/118
Patch01:	0001-Remove-configparser-backport.patch

BuildArch:		noarch

BuildRequires:	python2-devel
BuildRequires:	python-setuptools
BuildRequires:	python-sphinx
BuildRequires:	python-sphinx-theme-bootstrap
BuildRequires:	numpy
BuildRequires:	python-requests
BuildRequires:	pytest
%if 0%{?el6}
BuildRequires:	python-unittest2
BuildRequires:	python-importlib
BuildRequires:	python-argparse
%endif # if el6
%if 0%{?with_python3}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-sphinx
BuildRequires:	python3-sphinx-theme-bootstrap
BuildRequires:	python3-numpy
BuildRequires:	python3-requests
BuildRequires:	python3-pytest
%endif # if with_python3

%if 0%{?el6}
Requires:		python-importlib
Requires:		python-argparse
%endif # if el6

%description
python-future is the missing compatibility layer between Python 3 and Python 2.
It allows you to maintain a single, clean Python 3.x-compatible code base with
minimal cruft and run it easily on Python 2 mostly unchanged.

It provides future and past packages with back ports and forward ports of
features from Python 3 and 2. It also comes with futurize and pasteurize,
customized 2to3-based scripts that helps you to convert either Py2 or Py3 code
easily to support both Python 2 and 3 in a single clean Py3-style code base,
module by module.


%if 0%{?with_python3}
%package -n python3-%{srcname}
Summary:	Clean single-source support for Python 2/3
Group:		Development/Languages


%description -n python3-%{srcname}
python-future is the missing compatibility layer between Python 3 and Python 2.
It allows you to maintain a single, clean Python 3.x-compatible code base with
minimal cruft and run it easily on Python 2 mostly unchanged.

It provides future and past packages with back ports and forward ports of
features from Python 3 and 2. It also comes with futurize and pasteurize,
customized 2to3-based scripts that helps you to convert either Py2 or Py3 code
easily to support both Python 2 and 3 in a single clean Py3-style code base,
module by module.
%endif # with_python3


%prep
%setup -q -n %{srcname}-%{version}

%patch1 -p1

%if 0%{?with_python3}
rm -rf %{py3dir}
cp -a . %{py3dir}
find %{py3dir} -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python3}|'
%endif # with_python3

find -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python2}|'


%build
CFLAGS="%{optflags}" %{__python2} setup.py build

%if 0%{!?el6:1}
pushd docs
PYTHONPATH="../build/lib" CFLAGS="%{optflags}" make html
popd
%endif

%if 0%{?with_python3}
pushd %{py3dir}
CFLAGS="%{optflags}" %{__python3} setup.py build
pushd docs
PYTHONPATH="../build/lib" CFLAGS="%{optflags}" make html
popd
popd
%endif # with_python3


%install
# Must do the python3 install first because the scripts in /usr/bin are
# overwritten with every setup.py install (and we want the python2 version
# to be the default for now).
%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py install --skip-build --root %{buildroot}
popd
%endif # with_python3

%{__python2} setup.py install --skip-build --root %{buildroot}


%check
PYTHONPATH="%{buildroot}%{python2_sitelib}" \
	py.test

%if 0%{?with_python3}
pushd %{py3dir}
PYTHONPATH="%{buildroot}%{python3_sitelib}" \
	py.test-%{python3_version}
popd
%endif # with_python3


%files
%if 0%{?el6}
%doc README.rst LICENSE.txt
%else
%doc README.rst LICENSE.txt docs/build/html
%endif
%{python2_sitelib}/*
%{_bindir}/futurize
%{_bindir}/pasteurize


%if 0%{?with_python3}
%files -n python3-%{srcname}
%if 0%{?el6}
%doc README.rst LICENSE.txt
%else
%doc README.rst LICENSE.txt docs/build/html
%endif
%{python3_sitelib}/*
%endif # with_python3


%changelog
* Fri Sep 11 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.15.2-1
- New upstream release

* Sat Jul 25 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.15.0-1
- New upstream release

* Wed May 27 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.14.3-1
- New upstream release

* Sat Nov 22 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.14.2-1
- New upstream release
- Run tests against installed version instead of built version

* Fri Oct 03 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.14.1-2
- Fix build issue with documentation
- Fix build issue with tests
- Add python(3)-requests as a BR for testing
- Disable documentation on EL 6, which doesn't work easily

* Thu Oct 02 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.14.1-1
- New upstream release that should fix build issues

* Thu Oct 02 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.14.0-1
- New upstream release

* Tue Sep 23 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.13.1-1
- New upstream release

* Wed Aug 27 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.13.0-2
- Fix futurize script parameters

* Wed Aug 13 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.13.0-1
- New upstream release

* Tue Jul 22 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.12.4-2
- Better patch for removing six
- Cleanup EPEL5 stuff

* Sat Jul 19 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.12.4-1
- New upstream release

* Thu Jul 17 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.12.3-4
- Patch for upcasting behaviour

* Fri Jun 20 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.12.3-3
- Disable sphinx.ext.viewcode on EPEL 6

* Fri Jun 20 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.12.3-2
- Cherry-pick future.utils.__all__ patch

* Thu Jun 19 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.12.3-1
- New upstream release incorporating most patches
- Add documentation to package

* Thu Jun 19 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.12.2-4
- Cherry-pick fixes for tests
- Add dependencies for Python 2.6 (EPEL 6)

* Wed Jun 18 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.12.2-3
- Unbundle python-six
- Fix script packaging
- Patch broken formatting test on 3.4

* Tue Jun 17 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.12.2-2
- Use GitHub URL for source
- Fix rpmlint warnings
- Add BR for setuptools, numpy, requests
- Patch broken networking test
- Patch broken past standard_library test

* Sat Jun 14 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.12.2-1
- Initial RPM release

