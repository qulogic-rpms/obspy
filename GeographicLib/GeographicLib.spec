#global commit 2a6ccdf99f9b580aff4ef4725720172235fa9da0
#global shortcommit %(c=%{commit}; echo ${c:0:7})

%if 0%{?commit:1}
%global source_folder geographiclib-code-%{commit}
%else
%global source_folder %{name}-%{version}
%endif

# For octave scriptlets
%global octpkg geographiclib

Name:           GeographicLib
Version:        1.48
Release:        2%{?commit:.git%{shortcommit}}%{?dist}
Summary:        Library for geographic coordinate transformations 

License:        MIT
URL:            http://geographiclib.sourceforge.net/
%if 0%{?commit:1}
Source0:        https://sourceforge.net/code-snapshots/git/g/ge/geographiclib/code.git/geographiclib-code-%{commit}.zip
%else
Source0:        http://downloads.sourceforge.net/geographiclib/%{name}-%{version}.tar.gz 
%endif
# Use PythonInterp to set the python version for installation directory,
# and install python lib to arch-independent path
# Not yet submitted upstream
Patch0:         %{name}-1.48-python.patch
# Bundle MATLAB scripts into octave packages
# Not yet submitted upstream
Patch1:         %{name}-1.48-octave.patch
# Install nodejs files to arch-independent path
Patch2:         %{name}-1.48-nodejs.patch
# Fix test failure on i686
Patch3:         %{name}-1.48-test.patch

BuildRequires:  cmake
BuildRequires:  doxygen
BuildRequires:  gcc-c++
BuildRequires:  nodejs-packaging
BuildRequires:  octave-devel
BuildRequires:  python2-devel
BuildRequires:  python3-devel

%description
GeographicLib is a small set of C++ classes for performing conversions 
between geographic, UTM, UPS, MGRS, geocentric, and local Cartesian 
coordinates, for gravity (e.g., EGM2008), geoid height and geomagnetic 
field (e.g., WMM2010) calculations, and for solving geodesic problems. 
The emphasis is on returning accurate results with errors close to round-off 
(about 5–15 nanometers). New accurate algorithms for Geodesics on an 
ellipsoid of revolution and Transverse Mercator projection have been 
developed for this library. The functionality of the library can be accessed 
from user code, from the Utility programs provided, or via the 
Implementations in other languages.


%package devel
Summary:        Development files and libraries for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       cmake

%description devel
This package contains the header files and libraries
for %{name}. If you like to develop programs using %{name},
you will need to install %{name}-devel.


%package doc
Summary:        Development documentation for %name
BuildArch:      noarch

%description doc
This package contains doxygen-generated html API documentation for
the %{name} library.


%package -n python2-%{name}
Summary:        Python 2 implementation of %{name}
BuildArch:      noarch
%{?python_provide:%python_provide python2-%{name}}

%description -n python2-%{name}
A translation of the GeographicLib::Geodesic class to Python 2


%package -n python3-%{name}
Summary:        Python 3 implementation of %{name}
BuildArch:      noarch
%{?python_provide:%python_provide python3-%{name}}

%description -n python3-%{name}
A translation of the GeographicLib::Geodesic class to Python 3


%package -n octave-%{name}
Summary:        Octave implementation of %{name}
BuildArch:      noarch
Requires:         octave >= 3.4
Requires(post):   octave
Requires(postun): octave 

%description -n octave-%{name}
A translation of some of the GeographicLib C++ functionality to Octave


%package -n nodejs-%{name}
Summary:        NodeJS implementation of %{name}
BuildArch:      noarch
Requires:       nodejs

%description -n nodejs-%{name}
A translation of some of the GeographicLib C++ functionality to NodeJS


%prep
%autosetup -p1 -n %{source_folder}


%build
mkdir build; pushd build;
%cmake .. \
  -DGEOGRAPHICLIB_DATA="%{_datadir}/%{name}" \
  -DCOMMON_INSTALL_PATH=ON \
  -DGEOGRAPHICLIB_DOCUMENTATION=ON \
  -DUSE_RPATH=OFF \
  -DCMAKE_SKIP_INSTALL_RPATH=ON
%make_build
popd

mkdir build_python3; pushd build_python3;
%cmake .. \
  -DGEOGRAPHICLIB_DATA="%{_datadir}/%{name}" \
  -DCOMMON_INSTALL_PATH=ON \
  -DGEOGRAPHICLIB_DOCUMENTATION=ON \
  -DUSE_RPATH=OFF \
  -DCMAKE_SKIP_INSTALL_RPATH=ON \
  -DPython_ADDITIONAL_VERSIONS=3
%make_build
popd


%install
%make_install -C build
%make_install -C build_python3/python/geographiclib
rm -rf %{buildroot}/%{_datadir}/doc
rm -rf %{buildroot}/%{_libexecdir}

mkdir -p %{buildroot}%{_datadir}/%{name}


%check
make -C build test


%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%post -n octave-%{name}
%octave_cmd pkg rebuild

%preun -n octave-%{name}
%octave_pkg_preun

%postun -n octave-%{name}
%octave_cmd pkg rebuild


%files
%doc AUTHORS NEWS
%license LICENSE.txt
%{_bindir}/CartConvert
%{_bindir}/ConicProj
%{_bindir}/GeoConvert
%{_bindir}/GeodSolve
%{_bindir}/GeodesicProj
%{_bindir}/GeoidEval
%{_bindir}/Gravity
%{_bindir}/MagneticField
%{_bindir}/Planimeter
%{_bindir}/RhumbSolve
%{_bindir}/TransverseMercatorProj
%{_sbindir}/geographiclib-get-geoids
%{_sbindir}/geographiclib-get-gravity
%{_sbindir}/geographiclib-get-magnetic
%{_libdir}/libGeographic.so.17*
%{_datadir}/%{name}
%{_mandir}/man1/*.1.*
%{_mandir}/man8/*.8.*

%files devel
%{_includedir}/%{name}/
%{_libdir}/libGeographic.so
%{_libdir}/cmake/GeographicLib
%{_libdir}/pkgconfig/geographiclib.pc

%files doc
%license LICENSE.txt
%doc build/doc/html

%files -n python2-%{name}
%license LICENSE.txt
%{python2_sitelib}/geographiclib/

%files -n python3-%{name}
%license LICENSE.txt
%{python3_sitelib}/geographiclib/

%files -n octave-%{name}
%license LICENSE.txt
%{_datadir}/octave/packages/%{octpkg}-%{version}/

%files -n nodejs-%{name}
%license LICENSE.txt
%{nodejs_sitelib}/geographiclib/


%changelog
* Thu Jun 15 2017 Orion Poplawski <orion@cora.nwra.com> - 1.48-2
- Define %%octpkg for octave scriptlets

* Wed Jun 07 2017 Sandro Mani <manisandro@gmail.com> - 1.48-1
- Update to 1.48

* Fri Feb 10 2017 Fedora Release Engineering <releng@fedoraproject.org> - 1.43-9
- Rebuilt for https://fedoraproject.org/wiki/Fedora_26_Mass_Rebuild

* Mon Dec 19 2016 Miro Hrončok <mhroncok@redhat.com> - 1.43-8
- Rebuild for Python 3.6

* Wed Dec 07 2016 Orion Poplawski <orion@cora.nwra.com> - 1.43-7
- Rebuild for octave 4.2

* Tue Jul 19 2016 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.43-6
- https://fedoraproject.org/wiki/Changes/Automatic_Provides_for_Python_RPM_Packages

* Wed Feb 03 2016 Fedora Release Engineering <releng@fedoraproject.org> - 1.43-5
- Rebuilt for https://fedoraproject.org/wiki/Fedora_24_Mass_Rebuild

* Tue Nov 10 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.43-4
- Rebuilt for https://fedoraproject.org/wiki/Changes/python3.5

* Fri Jul 3 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.43-3
- Add Python 3 subpackage

* Tue Jun 16 2015 Fedora Release Engineering <rel-eng@lists.fedoraproject.org> - 1.43-2
- Rebuilt for https://fedoraproject.org/wiki/Fedora_23_Mass_Rebuild

* Fri May 22 2015 Rich Mattes <richmattes@gmail.com> - 1.43-1
- Update to release 1.43

* Mon May 04 2015 Rich Mattes <richmattes@gmail.com> - 1.42-1
- Update to release 1.42
- Add octave subpackage

* Sat May 02 2015 Kalev Lember <kalevlember@gmail.com> - 1.40-2
- Rebuilt for GCC 5 C++11 ABI change

* Fri Jan 02 2015 Rich Mattes <richmattes@gmail.com> - 1.40-1
- Update to release 1.40

* Sat Oct 04 2014 Rich Mattes <richmattes@gmail.com> - 1.38-2
- Fix cmake installation directory

* Sat Oct 04 2014 Rich Mattes <richmattes@gmail.com> - 1.38-1
- Update to 1.38
- Change BR from python2 to python2-devel
- Remove buildroot cleanup from install section

* Fri Sep 19 2014 Rich Mattes <richmattes@gmail.com> - 1.37-1
- Initial package
