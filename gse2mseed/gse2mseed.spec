Name:		gse2mseed
Version:	1.13
Release:	2%{?dist}
License:	GPLv3+
Summary:	Convert GSE 2.x and IMS 1.0 time series data to miniSEED
Url:		https://github.com/iris-edu/gse2mseed
Source0:	https://github.com/iris-edu/%{name}/archive/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:	gcc
BuildRequires:	libmseed-devel >= 2.19.3

%description
Convert GSE 2.x and IMS 1.0 time series data to miniSEED.

The GSE samples may be in either INT and CM6 formats.


%prep
%autosetup -n %{name}-%{version}

# Remove bundled copy.
rm -rf libmseed


%build
pushd src
# Disable internal library copy by overriding REQCFLAGS and LDFLAGS
make %{?_smp_mflags} \
	CC=gcc CFLAGS="%{optflags}" \
	REQCFLAGS='' LDFLAGS='' \
	all
popd


%install
mkdir -p %{buildroot}%{_bindir}
cp -p gse2mseed %{buildroot}%{_bindir}

mkdir -p %{buildroot}%{_mandir}/man1
cp -p doc/*.1 %{buildroot}%{_mandir}/man1/


%files
%doc README.md ChangeLog
%license LICENSE
%{_bindir}/gse2mseed
%{_mandir}/man1/*


%changelog
* Sun Dec 09 2018 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.13-2
- Add explicit gcc BuildRequires.

* Wed Jun 7 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.13-1
- New upstream release

* Thu Feb 18 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.12-2
- Rebuild for Rawhide

* Thu Aug 21 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.12-1
- Initial package release
