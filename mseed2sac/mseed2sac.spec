Name:		mseed2sac
Version:	2.1
Release:	2%{?dist}
License:	GPLv3+
Summary:	A program to convert miniSEED data to SAC format
Url:		https://github.com/iris-edu/mseed2sac
Source0:	https://github.com/iris-edu/%{name}/archive/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:	gcc
BuildRequires:	zlib-devel
BuildRequires:	libmseed-devel >= 2.19.3

%description
A program to convert miniSEED data to SAC format.

An optional metadata file may be supplied that contains values for the SAC
headers that are not available in Mini-SEED. An event location may be
supplied on the command line for placement into the SAC headers.


%prep
%autosetup -n %{name}-%{version}

# Remove bundled copy.
rm -rf libmseed


%build
pushd src
# Disable internal library copy by overriding REQCFLAGS and LDFLAGS
make %{?_smp_mflags} \
	CC=gcc CFLAGS="%{optflags}" \
	REQCFLAGS='' LDFLAGS='' \
	all
popd


%install
mkdir -p %{buildroot}%{_bindir}
cp -p mseed2sac %{buildroot}%{_bindir}

mkdir -p %{buildroot}%{_mandir}/man1
cp -p doc/*.1 %{buildroot}%{_mandir}/man1/


%files
%doc README.md ChangeLog
%license LICENSE
%{_bindir}/mseed2sac
%{_mandir}/man1/*

%changelog
* Sun Dec 09 2018 Elliott Sales de Andrade <quantum.analyst@gmail.com> 2.1-2
- Add explicit gcc BuildRequires.

* Thu Jun 08 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 2.1-1
- New upstream release

* Thu Feb 18 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.0-2
- Rebuild for Rawhide

* Wed Aug 20 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2.0-1
- Initial package release
