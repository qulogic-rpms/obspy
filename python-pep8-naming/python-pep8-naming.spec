%global srcname pep8-naming
%global srcname_ pep8_naming
%global _description \
Check the PEP-8 naming conventions. \
This module provides a plugin for flake8, the Python code checker. \
(It replaces the plugin flint-naming for the flint checker.)


Name:           python-%{srcname}
Version:        0.4.1
Release:        3
Summary:        Check PEP-8 naming conventions, a plugin for flake8

License:        MIT
URL:            https://pypi.python.org/pypi/%{srcname}
Source0:        https://files.pythonhosted.org/packages/source/p/%{srcname}/%{srcname}-%{version}.tar.gz

BuildArch:      noarch

BuildRequires:  python2-devel
BuildRequires:  python-flake8

BuildRequires:  python3-devel
BuildRequires:  python3-flake8


%description %{_description}


%package -n python2-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python2-%{srcname}}
Requires:       python-flake8


%description -n python2-%{srcname} %{_description}


%package -n python3-%{srcname}
Summary:        %{summary}
%{?python_provide:%python_provide python3-%{srcname}}
Requires:       python3-flake8


%description -n python3-%{srcname} %{_description}


%prep
%autosetup -n %{srcname}-%{version}


%build
%py2_build
%py3_build


%install
%py2_install
%py3_install


%check
%{__python2} run_tests.py
%{__python3} run_tests.py


%files -n python2-%{srcname}
%doc README.rst
%{python2_sitelib}/pep8ext_naming.py*
%{python2_sitelib}/%{srcname_}-%{version}-py?.?.egg-info

%files -n python3-%{srcname}
%doc README.rst
%{python3_sitelib}/pep8ext_naming.py
%{python3_sitelib}/__pycache__/pep8ext_naming.*.py*
%{python3_sitelib}/%{srcname_}-%{version}-py?.?.egg-info


%changelog
* Wed Oct 25 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.4.1-3
- Enable tests during build.
- Simplify spec for Fedora.

* Thu Jun 08 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.4.1-2
- Don't build python3-pep8-naming on EL7

* Tue Jun 6 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.4.1-1
- New upstream release

* Tue Feb 16 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.3.3-1
- Initial RPM release
