%if 0%{?fedora} > 12
%global with_python3 1
%else
%{!?__python2: %global __python2 /usr/bin/python2}
%{!?python2_sitelib: %global python2_sitelib %(%{__python2} -c "from distutils.sysconfig import get_python_lib; print (get_python_lib())")}
%endif

%global srcname sphinx-bootstrap-theme

Name:		python-sphinx-theme-bootstrap
Version:	0.4.8
Release:	1%{?dist}
Summary:	Sphinx Bootstrap Theme

License:	MIT and ASL 2.0
URL:		http://ryan-roemer.github.io/sphinx-bootstrap-theme/README.html
Source0:	https://pypi.python.org/packages/source/s/%{srcname}/%{srcname}-%{version}.tar.gz

BuildArch:		noarch

BuildRequires:	python2-devel
BuildRequires:	python-setuptools
%if 0%{?with_python3}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
%endif

Requires:		python-sphinx

%description
This Sphinx theme integrates the Twitter Bootstrap CSS / JavaScript framework
with various layout options, hierarchical menu navigation, and mobile-friendly
responsive design. It is configurable, extensible and can use any number of
different Bootswatch CSS themes.


%if 0%{?with_python3}
%package -n python3-sphinx-theme-bootstrap
Summary:	Sphinx Bootstrap Theme
Requires:	python3-sphinx

%description -n python3-sphinx-theme-bootstrap
This Sphinx theme integrates the Twitter Bootstrap CSS / JavaScript framework
with various layout options, hierarchical menu navigation, and mobile-friendly
responsive design. It is configurable, extensible and can use any number of
different Bootswatch CSS themes.
%endif # with_python3


%prep
%setup -q -n %{srcname}-%{version}

%if 0%{?with_python3}
rm -rf %{py3dir}
cp -a . %{py3dir}
find %{py3dir} -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python3}|'
%endif # with_python3

find -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python2}|'


%build
CFLAGS="$RPM_OPT_FLAGS" %{__python2} setup.py build

%if 0%{?with_python3}
pushd %{py3dir}
CFLAGS="$RPM_OPT_FLAGS" %{__python3} setup.py build
popd
%endif # with_python3


%install
rm -rf %{buildroot}

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py install --skip-build --root $RPM_BUILD_ROOT
popd
%endif # with_python3

%{__python2} setup.py install --skip-build --root $RPM_BUILD_ROOT


%clean
rm -rf $RPM_BUILD_ROOT


%files
%doc README.rst LICENSE.txt
%{python2_sitelib}/*

%if 0%{?with_python3}
%files -n python3-sphinx-theme-bootstrap
%doc README.rst LICENSE.txt
%{python3_sitelib}/*
%endif # with_python3


%changelog
* Tue Feb 16 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 0.4.8-1
- New upstream release

* Fri Jun 20 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 0.4.0-3
- Add documentation
- Correct license

* Fri Jun 20 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 0.4.0-2
- Fix typo that broke EPEL 6 build

* Thu Jun 19 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 0.4.0-1
- Initial RPM release
