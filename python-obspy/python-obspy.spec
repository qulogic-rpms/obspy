%bcond_with doc

%global srcname obspy

Name:           python-%{srcname}
Version:        1.4.0
Release:        2%{?dist}
Summary:        Python toolbox for seismology/seismological observatories

License:        LGPL-3.0-only
URL:            https://www.obspy.org
Source0:        https://github.com/obspy/obspy/archive/%{version}/%{name}-%{version}.tar.gz
# pytest-jsonreport is not available, and we aren't reporting things anyway.
Patch:          0001-Only-check-for-pytest_jsonreport-if-reporting.patch
Patch:          0002-Skip-doctests-that-use-the-network.patch

BuildRequires:  gcc

%description
ObsPy is an open-source project dedicated to provide a Python framework for
processing seismological data. It provides parsers for common file formats,
clients to access data centers and seismological signal processing routines
which allow the manipulation of seismological time series (see Beyreuther
et al. 2010, Megies et al. 2011).

The goal of the ObsPy project is to facilitate rapid application development
for seismology.


%package -n     python3-%{srcname}
Summary:        %{summary}

BuildRequires:  python3-devel

# Tests
BuildRequires:  natural-earth-map-data-110m
BuildRequires:  natural-earth-map-data-50m
BuildRequires:  python3dist(packaging)
BuildRequires:  python3dist(pyproj)
BuildRequires:  python3dist(pytest)

# Documentation
%if %{with doc}
BuildRequires:  help2man
BuildRequires:  python3dist(sphinx)
BuildRequires:  python3dist(sphinx-bootstrap-theme)
BuildRequires:  python3dist(pybtex)
%endif

%description -n python3-%{srcname}
ObsPy is an open-source project dedicated to provide a Python framework for
processing seismological data. It provides parsers for common file formats,
clients to access data centers and seismological signal processing routines
which allow the manipulation of seismological time series (see Beyreuther
et al. 2010, Megies et al. 2011).

The goal of the ObsPy project is to facilitate rapid application development
for seismology.


%if %{with doc}
%package doc
Summary:        %{summary}

%description doc
Documentation for ObsPy.
%endif


%prep
%autosetup -n %{srcname}-%{version} -p1

echo %{version} > obspy/RELEASE-VERSION

chmod -x \
    obspy/taup/tests/data/TauP_test_data/gendata.sh \
    obspy/io/mseed/src/libmseed/test/*.test


%generate_buildrequires
%pyproject_buildrequires -rx geo,imaging,io.shapefile


%build
%pyproject_wheel

%if %{with doc}
%{python3} setup.py build_man
%endif


%install
%pyproject_install
%pyproject_save_files %{srcname}

%if %{with doc}
%{python3} setup.py install_man --skip-build --root %{buildroot}
%endif

%if %{with doc}
pushd misc/docs
PYTHONPATH="%{buildroot}%{python3_sitearch}" make html
rm build/html/.buildinfo
popd
%endif

%check
PYTHONPATH="%{buildroot}%{python3_sitearch}" \
    PATH="%{buildroot}%{_bindir}:$PATH" \
    PYTHONDONTWRITEBYTECODE=1 \
        obspy-runtests --no-report -p no:cacheprovider


%if %{with doc}
%files doc
%doc misc/docs/build/html
%endif


%files -n python3-%{srcname} -f %{pyproject_files}
%doc README.md CHANGELOG.txt CONTRIBUTING.md
%license LICENSE.txt

%{_bindir}/obspy-dataless2resp
%{_bindir}/obspy-dataless2xseed
%{_bindir}/obspy-flinn-engdahl
%{_bindir}/obspy-mopad
%{_bindir}/obspy-mseed-recordanalyzer
%{_bindir}/obspy-plot
%{_bindir}/obspy-print
%{_bindir}/obspy-reftek-rescue
%{_bindir}/obspy-runtests
%{_bindir}/obspy-scan
%{_bindir}/obspy-sds-report
%{_bindir}/obspy-xseed2dataless

%if %{with doc}
%{_mandir}/man1/obspy-dataless2resp.1*
%{_mandir}/man1/obspy-dataless2xseed.1*
%{_mandir}/man1/obspy-flinn-engdahl.1*
%{_mandir}/man1/obspy-mopad.1*
%{_mandir}/man1/obspy-mseed-recordanalyzer.1*
%{_mandir}/man1/obspy-plot.1*
%{_mandir}/man1/obspy-print.1*
%{_mandir}/man1/obspy-reftek-rescue.1*
%{_mandir}/man1/obspy-runtests.1*
%{_mandir}/man1/obspy-scan.1*
%{_mandir}/man1/obspy-sds-report.1*
%{_mandir}/man1/obspy-xseed2dataless.1*
%endif


%changelog
* Sun Nov 12 2023 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.4.0-2
- Rebuild for Python 3.12 in Fedora 39

* Mon Nov 21 2022 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.4.0-1
- Update to 1.4.0
- Remove Python3 aliases of commands

* Mon Oct 17 2022 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.3.1-1
- Update to 1.3.1 (quantum.analyst@gmail.com)
- Update to 1.3.0 (quantum.analyst@gmail.com)

* Mon Jan 24 2022 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.2.2-4
- Fix Basemap + Matplotlib 3.4 compatibility

* Mon Jan 24 2022 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.2.2-3
- Fix GCF parsing on 32-bit systems w/ latest NumPy
- Fix incorrect byte swapping in mseed

* Sat Jan 22 2022 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.2.2-2
- Remove tests for deprecated modules 
- Backport new Python compatibility fixes 
- Backport other warning fixes 
- Backport NumPy & SciPy compatibility fixes 
- Backport Matplotlib compat patches 
- Drop NumPy compat wrapper that breaks with 1.22 

* Mon Aug 10 2020 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.2.2-1
- Update obspy to 1.2.2
- Fix unnecessary executable bits
- Use bundled libmseed again
- Remove extra broken comments

* Fri May 03 2019 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.1.1-5
- Workaround copr using a single srpm.

* Fri May 03 2019 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.1.1-4
- Don't patch autosummary on Fedora <30.

* Fri May 03 2019 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.1.1-3
- Fix build against Sphinx 2.
- Fix documentation build with pybtex 0.22.0+.

* Thu May 02 2019 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.1.1-2
- Split apart Python 2 and 3 subpackages.
- Fix files in Python 2 subpackage.
- Remove now-unused patch.

* Wed May 01 2019 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.1.1-1
- Update to 1.1.1.
- Switch to tag archive URL.

* Wed May 01 2019 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.1.0-4
- Setup python2 subpackages. (quantum.analyst@gmail.com)
- Remove old conditions. (quantum.analyst@gmail.com)

* Sun Dec 09 2018 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.1.0-3
- Ignore test failures for now.

* Sat Nov 17 2018 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.1.0-2
- Backport patch to fix Sphinx 1.7.
- Add explicit gcc BR.

* Fri Oct 27 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.1.0-1
- Update to latest release.
- Stop unbundling evalresp.

* Fri Jul 14 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.3-2
- Patch in support for Matplotlib 2.

* Fri Mar 3 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.3-1
- New upstream release

* Wed Oct 19 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.2-3
- Fix compatibility with newer libmseed
- Patch several mseed issues

* Thu Sep 22 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.2-2
- Fix broken entry point

* Wed Aug 3 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.2-1
- New upstream release

* Sat Mar 26 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.1-2
- Remove gdal-python3 BR for Fedora 22, which doesn't support it
- Fix build against NumPy 1.11

* Sat Mar 26 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.1-1
- New upstream release

* Sun Feb 28 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.0-3
- Update future dependency to match official Fedora/EPEL naming

* Sat Feb 20 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.0-2
- Build documentation on Python 2 again

* Sat Feb 20 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.0-1
- New upstream release
- Make Python 3 the default binary if available
- Build documentation with Python 3 if available
- Add python-mock and python-nose to requirements since matplotlib falsely needs it
- Stop building documentation against mlpy
- Remove suds-jurko dependency

* Sat May 16 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.10.2-2
- Add BR on python-pybtex
- Patch to build with NumPy 1.4

* Fri May 15 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.10.2-1
- New upstream release

* Fri Mar 20 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.10.1-1
- New upstream release

* Thu Mar 19 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.10.0-2
- Fix error in stored version

* Thu Mar 19 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.10.0-1
- Initial RPM release
