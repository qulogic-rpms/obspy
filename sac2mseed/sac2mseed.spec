Name:		sac2mseed
Version:	1.12
Release:	2%{?dist}
License:	GPLv3+
Summary:	Convert SAC data to miniSEED format
Url:		https://github.com/iris-edu/sac2mseed
Source0:	https://github.com/iris-edu/%{name}/archive/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:	gcc
BuildRequires:	libmseed-devel >= 2.19.3

%description
Convert SAC time series data to miniSEED format.

NOTE: SAC data files contain header fields with no equivalents in miniSEED
format, these fields are lost during conversion. An optional metadata output
file can be created during conversion that contains the most important
metadata.


%prep
%autosetup -n %{name}-%{version}

# Remove bundled copy.
rm -rf libmseed


%build
pushd src
# Disable internal library copy by overriding REQCFLAGS and LDFLAGS
make %{?_smp_mflags} \
	CC=gcc CFLAGS="%{optflags}" \
	REQCFLAGS='' LDFLAGS='' \
	all
popd


%install
mkdir -p %{buildroot}%{_bindir}
cp -p sac2mseed %{buildroot}%{_bindir}

mkdir -p %{buildroot}%{_mandir}/man1
cp -p doc/*.1 %{buildroot}%{_mandir}/man1/


%files
%doc README.md ChangeLog
%license LICENSE
%{_bindir}/sac2mseed
%{_mandir}/man1/*


%changelog
* Sun Dec 09 2018 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.12-2
- Add explicit gcc BuildRequires.

* Thu Jun 08 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.12-1
- New upstream release

* Thu Feb 18 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.11-2
- Rebuild for Rawhide

* Thu Aug 21 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.11-1
- Initial package release
