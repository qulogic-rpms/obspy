Name:		msi
Version:	3.7
Release:	1%{?dist}
License:	GPLv3+
Summary:	A general purpose tool for parsing Mini-SEED records
Url:		https://github.com/iris-edu/msi
Source0:	https://github.com/iris-edu/%{name}/archive/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:	libmseed-devel >= 2.19.4

%description
The miniSEED inspector parses and reports details from SEED formatted data
records.

This program is using for summarizing data and diagnosing issues with miniSEED.
In addition to parsing and printing details of individual records, msi will
reconstruct continuous time series, print data values, save data values in
binary, etc. Numerous options make this utility very flexible for quickly
viewing details and summaries of miniSEED formatted data.


%prep
%autosetup -n %{name}-%{version}

# Remove bundled copy.
rm -rf libmseed


%build
pushd src
# Disable internal library copy by overriding GCCFLAGS, REQCFLAGS and LDFLAGS
make %{?_smp_mflags} \
	CC=gcc CFLAGS="%{optflags}" GCCFLAGS="%{optflags}" \
	REQCFLAGS='' LDFLAGS='' \
	all
popd


%install
mkdir -p %{buildroot}%{_bindir}
cp -p msi %{buildroot}%{_bindir}

mkdir -p %{buildroot}%{_mandir}/man1
cp -p doc/*.1 %{buildroot}%{_mandir}/man1/


%files
%doc README.md ChangeLog
%license LICENSE
%{_bindir}/msi
%{_mandir}/man1/*


%changelog
* Thu Jun 08 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 3.7-1
- New upstream release

* Thu Feb 18 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 3.6-2
- Rebuild for Rawhide

* Thu Mar 12 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 3.6-1
- New upstream release

* Wed Aug 20 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 3.5-1
- Initial package release
