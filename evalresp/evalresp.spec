Name:		evalresp
Version:	3.3.3
Release:	4%{?dist}
License:	LGPLv3+
Summary:	Evaluate response information using RESP files
Url:		http://www.iris.edu/ds/nodes/dmc/software/downloads/evalresp
Group:		Development/Libraries
Source0:	http://www.iris.edu/pub/programs/evalresp-%{version}.tar.gz
# https://github.com/obspy/obspy/issues/827
Source1:	LICENSE.txt
Patch00:	evalresp-no-extra-install.patch
Patch01:	0001-evalresp-First-somewhat-valid-response-curve.patch
Patch02:	0002-Small-evalresp-fix.patch
Patch03:	0003-evalresp-Adding-MBAR-as-pressure-units.patch
Patch04:	0004-evalresp-Fix-realloc-bug.patch
Patch05:	0005-evalresp-Fix-set-but-unused-warnings.patch
Patch06:	0006-evalresp-Remove-unnecessary-memset.patch
Patch07:	0007-evalresp-Fix-some-uninitialized-variable.patch
Patch08:	0008-evalresp-Fix-warnings-about-getcwd.patch
Patch09:	0009-evalresp-Fix-unused-return-value-warnings.patch
Patch10:	0010-evalresp-Fix-parenthesis-warning.patch
Patch11:	0011-evalresp-Fix-signed-unsigned-comparison-warning.patch
Patch12:	0012-evalresp-Fix-strict-prototypes.patch
Patch13:	0013-evalresp-Avoid-segfault-by-declaring-a-variable-to-be-volatil.patch
Patch14:	0014-evalresp-signal-Fix-one-uninitialized-conditional-jump.patch
Patch15:	0015-signal-Fix-invalid-strncpy-usage-in-evalresp.patch
Patch16:	0016-signal-Fix-invalid-read-in-evalresp.patch
Patch17:	0017-evalresp-Fix-non-UTF8-characters-in-README.patch
# https://github.com/obspy/obspy/pull/892
Patch18:	0018-evalresp-Remove-GPL-code.patch

%package devel
Summary:	Evaluate response information using RESP files
Group:		Development/Libraries
Requires:	%{name}%{?_isa} = %{version}-%{release}

BuildRequires:	autoconf
BuildRequires:	automake
BuildRequires:	libtool


%description
evalresp can be used to calculate either the complex spectral response or the
frequency-amplitude-phase response for a specified station or set of stations,
channel or channels, and network, for a specified date, time and frequency,
using the SEED ASCII response (RESP) files produced by the program rdseed as
input.

%description devel
Development files for %{name} library.


%prep
%setup -q
cp -p %SOURCE1 .

%patch0 -p0
%patch1 -p5
%patch2 -p5
%patch3 -p5
%patch4 -p5
%patch5 -p5
%patch6 -p5
%patch7 -p5
%patch8 -p5
%patch9 -p5
%patch10 -p5
%patch11 -p5
%patch12 -p5
%patch13 -p5
%patch14 -p5
%patch15 -p5
%patch16 -p5
%patch17 -p5
%patch18 -p1


%build
autoreconf -vfi
%configure --enable-shared --disable-static

make %{?_smp_mflags}


%install
make install DESTDIR=%{buildroot}

# No libtool stuff
rm %{buildroot}%{_libdir}/libevresp.la

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig


%files
%doc README ChangeLog LICENSE.txt
%{_bindir}/evalresp
%{_libdir}/libevresp.so.0
%{_libdir}/libevresp.so.0.0.0
%{_mandir}/man1/*

%files devel
%{_includedir}/evresp.h
%{_libdir}/libevresp.so

%changelog
* Thu Feb 18 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 3.3.3-4
- Rebuild for Rawhide

* Fri Mar 06 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 3.3.3-3
- Fix Requires for -devel package.

* Tue Oct 21 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 3.3.3-2
- Replace GPL code with LGPL code
- Fix build issue with autotools in mock

* Wed Aug 20 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 3.3.3-1
- Initial package release
