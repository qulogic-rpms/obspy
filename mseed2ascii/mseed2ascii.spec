Name:		mseed2ascii
Version:	2.1
Release:	2%{?dist}
License:	GPLv3+
Summary:	Convert miniSEED formatted data to ASCII
Url:		https://github.com/iris-edu/mseed2ascii
Source0:	https://github.com/iris-edu/%{name}/archive/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:	gcc
BuildRequires:	libmseed-devel >= 2.19.3

%description
Convert miniSEED formatted data to ASCII.


%prep
%autosetup -n %{name}-%{version}

# Remove bundled copy.
rm -rf libmseed


%build
pushd src
# Disable internal library copy by overriding REQCFLAGS and LDFLAGS
make %{?_smp_mflags} \
	CC=gcc CFLAGS="%{optflags}" \
	REQCFLAGS='' LDFLAGS='' \
	all
popd


%install
mkdir -p %{buildroot}%{_bindir}
cp -p mseed2ascii %{buildroot}%{_bindir}

mkdir -p %{buildroot}%{_mandir}/man1
cp -p doc/*.1 %{buildroot}%{_mandir}/man1/


%files
%doc README.md ChangeLog
%license LICENSE
%{_bindir}/mseed2ascii
%{_mandir}/man1/*


%changelog
* Sun Dec 09 2018 Elliott Sales de Andrade <quantum.analyst@gmail.com> 2.1-2
- Add explicit gcc BuildRequires.

* Thu Jun 08 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 2.1-1
- Initial package release
