%global download_id 538
Name:		ggp2mseed
Version:	1.1
Release:	3%{?dist}
License:	GPLv2+
Summary:	Convert GGP data to miniSEED format
Url:		https://seiscode.iris.washington.edu/projects/ggp2mseed
Group:		Applications/File
Source0:	https://seiscode.iris.washington.edu/attachments/download/%{download_id}/ggp2mseed-%{version}.tar.gz

BuildRequires:	gcc
BuildRequires:	libmseed-devel >= 2.12

%description
A converter for the Global Geodynamic Project's super gravimeter time series
data format to FDSN Mini-SEED.


%prep
%setup -q


%build
pushd src
# Disable internal library copy by overriding REQCFLAGS and LDFLAGS
make %{?_smp_mflags} \
	CC=gcc CFLAGS="%{optflags}" \
	REQCFLAGS='' LDFLAGS='' \
	all
popd


%install
mkdir -p %{buildroot}%{_bindir}
cp -p ggp2mseed %{buildroot}%{_bindir}


%files
%doc README ChangeLog
%{_bindir}/ggp2mseed

%changelog
* Sun Dec 09 2018 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.1-3
- Add explicit gcc BuildRequires.

* Thu Feb 18 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.1-2
- Rebuild for Rawhide

* Thu Aug 21 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.1-1
- Initial package release
