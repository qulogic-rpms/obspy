%if 0%{?fedora} > 12
%global with_python3 1
%else
%{!?__python2: %global __python2 /usr/bin/python2}
%{!?python2_sitelib: %global python2_sitelib %(%{__python2} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%endif

%global srcname pybtex
%global pybtex_progs pybtex pybtex-convert pybtex-format

Name:		python-%{srcname}
Version:	0.21
Release:	1%{?dist}
Summary:	A BibTeX-compatible bibliography processor in Python


Group:		Applications/Publishing
License:	MIT
URL:		http://pybtex.sourceforge.net
Source0:	https://pypi.python.org/packages/source/p/%{srcname}/%{srcname}-%{version}.tar.gz

BuildArch:	noarch

BuildRequires:	python2-devel
BuildRequires:	python-setuptools
BuildRequires:	PyYAML >= 3.01
BuildRequires:	python2-latexcodec >= 1.0.4

# Tests
BuildRequires:	python-nose

# Documentation
BuildRequires:	python-sphinx

%if 0%{?with_python3}
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-PyYAML >= 3.01
BuildRequires:	python3-latexcodec >= 1.0.4
BuildRequires:	python3-nose
%endif # if with_python3

Requires:	PyYAML >= 3.01
Requires:	python2-latexcodec >= 1.0.4


%description
Pybtex reads citation information from a file and produces a formatted
bibliography. BibTeX style files are supported. Alternatively it is possible
to write styles in Python.


%if 0%{?with_python3}
%package -n python3-%{srcname}
Summary:	A BibTeX-compatible bibliography processor in Python
Group:		Applications/Publishing

Requires:	python3-PyYAML >= 3.01
Requires:	python3-latexcodec >= 1.0.4


%description -n python3-%{srcname}
Pybtex reads citation information from a file and produces a formatted
bibliography. BibTeX style files are supported. Alternatively it is possible
to write styles in Python.
%endif # with_python3


%prep
%setup -q -n %{srcname}-%{version}

find pybtex -name '*.py' | xargs sed -i '1{\@^#!/usr/bin/env python@d}'

%if 0%{?with_python3}
rm -rf %{py3dir}
cp -a . %{py3dir}
find %{py3dir} -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python3}|'
%endif # with_python3

find -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python2}|'


%build
CFLAGS="%{optflags}" %{__python2} setup.py build

%if 0%{?with_python3}
pushd %{py3dir}
CFLAGS="%{optflags}" %{__python3} setup.py build
popd
%endif # with_python3


%install
# Must do the python3 install first because the scripts in /usr/bin are
# overwritten with every setup.py install (and we want the python2 version
# to be the default for now).
%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py install --skip-build --root %{buildroot}

# Unnecessary directory only required for lib2to3
rm -rf %{buildroot}%{python3_sitelib}/custom_fixers

# Create versioned binaries
pushd %{buildroot}%{_bindir}
for prog in %{pybtex_progs};
do
    mv ${prog} ${prog}-%{python3_version}
    ln -s ${prog}-%{python3_version} ${prog}-3
done
popd

# Install man pages
mkdir -p %{buildroot}%{_mandir}/man1
pushd docs/man1
for prog in %{pybtex_progs};
do
    cp ${prog}.1 %{buildroot}%{_mandir}/man1/${prog}-%{python3_version}.1
    ln -s ${prog}-%{python3_version}.1 %{buildroot}%{_mandir}/man1/${prog}-3.1
done
popd
popd
%endif # with_python3

%{__python2} setup.py install --skip-build --root %{buildroot}

# Unnecessary directory only required for lib2to3
rm -rf %{buildroot}%{python2_sitelib}/custom_fixers

# Create versioned binaries
pushd %{buildroot}%{_bindir}
for prog in %{pybtex_progs};
do
    mv ${prog} ${prog}-%{python2_version}
    ln -s ${prog}-%{python2_version} ${prog}-2
    ln -s ${prog}-2 ${prog}
done
popd

# Install man pages
mkdir -p %{buildroot}%{_mandir}/man1
pushd docs/man1
for prog in %{pybtex_progs};
do
    cp ${prog}.1 %{buildroot}%{_mandir}/man1/${prog}-%{python2_version}.1
    ln -s ${prog}-%{python2_version}.1 %{buildroot}%{_mandir}/man1/${prog}-2.1
    ln -s ${prog}-%{python2_version}.1 %{buildroot}%{_mandir}/man1/${prog}.1
done
popd

# Requires installation because of setuptools plugins.
pushd docs
PYTHONPATH="%{buildroot}%{python2_sitelib}" \
    make html
popd


%check
%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py nosetests
popd
%endif

%{__python2} setup.py nosetests


%files
%doc README CHANGES COPYING docs/build/html
%{_bindir}/pybtex
%{_bindir}/pybtex-convert
%{_bindir}/pybtex-format
%{_bindir}/pybtex-2
%{_bindir}/pybtex-convert-2
%{_bindir}/pybtex-format-2
%{_bindir}/pybtex-%{python2_version}
%{_bindir}/pybtex-convert-%{python2_version}
%{_bindir}/pybtex-format-%{python2_version}
%{python2_sitelib}/pybtex*
%{_mandir}/man1/pybtex.1*
%{_mandir}/man1/pybtex-convert.1*
%{_mandir}/man1/pybtex-format.1*
%{_mandir}/man1/pybtex-2.1*
%{_mandir}/man1/pybtex-convert-2.1*
%{_mandir}/man1/pybtex-format-2.1*
%{_mandir}/man1/pybtex-%{python2_version}.1*
%{_mandir}/man1/pybtex-convert-%{python2_version}.1*
%{_mandir}/man1/pybtex-format-%{python2_version}.1*


%if 0%{?with_python3}
%files -n python3-%{srcname}
%doc README CHANGES COPYING docs/build/html
%{_bindir}/pybtex-3
%{_bindir}/pybtex-convert-3
%{_bindir}/pybtex-format-3
%{_bindir}/pybtex-%{python3_version}
%{_bindir}/pybtex-convert-%{python3_version}
%{_bindir}/pybtex-format-%{python3_version}
%{python3_sitelib}/pybtex*
%{_mandir}/man1/pybtex-3.1*
%{_mandir}/man1/pybtex-convert-3.1*
%{_mandir}/man1/pybtex-format-3.1*
%{_mandir}/man1/pybtex-%{python3_version}.1*
%{_mandir}/man1/pybtex-convert-%{python3_version}.1*
%{_mandir}/man1/pybtex-format-%{python3_version}.1*
%endif # with_python3


%changelog
* Tue Jun 6 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.21-1
- New upstream release

* Sat May 16 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.18-1
- Initial RPM release
