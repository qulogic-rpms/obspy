%if 0%{?el7}
%{!?__python2: %global __python2 /usr/bin/python2}
%{!?python2_version: %global python2_version 2.6}
%{!?python2_sitelib: %global python2_sitelib %(%{__python2} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%{!?python2_sitearch: %global python2_sitearch %(%{__python2} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%endif

%if 0%{?el7}
%bcond_without doc
%else
%bcond_with doc
%endif

%global obspy_programs dataless2resp dataless2xseed flinn-engdahl indexer mopad mseed-recordanalyzer plot print reftek-rescue runtests scan sds-report xseed2dataless

%global srcname obspy

Name:		python2-%{srcname}
Version:	1.1.1
Release:	2%{?dist}
Summary:	A Python Toolbox for seismology/seismological observatories

Group:		Development/Libraries
License:	LGPLv3
URL:		https://www.obspy.org/
Source0:	https://github.com/obspy/obspy/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires:	gcc
BuildRequires:	libmseed-devel >= 2.19.4

%{?python_provide:%python_provide python2-obspy}

BuildRequires:	python2-devel
BuildRequires:	python-setuptools
BuildRequires:	python2-future >= 0.12.4
BuildRequires:	numpy >= 1.6.1
BuildRequires:	python-mock
BuildRequires:	scipy >= 0.9.0
BuildRequires:	python-lxml
%if 0%{?fedora}
BuildRequires:	python2-matplotlib
%else
BuildRequires:	python-matplotlib
%endif
BuildRequires:	python-basemap
BuildRequires:	python-basemap-data
BuildRequires:	python-basemap-data-hires
BuildRequires:	python-sqlalchemy
BuildRequires:	python-decorator
BuildRequires:	python-requests

# Tests
%if 0%{?fedora}
BuildRequires:	python-flake8 >= 2
BuildRequires:	pyproj
BuildRequires:	python-pep8-naming
BuildRequires:	gdal-python
%endif
BuildRequires:	python-nose

# Documentation
BuildRequires:	help2man
%if %{with doc}
BuildRequires:	python-sphinx
BuildRequires:	python2-sphinx-bootstrap-theme
BuildRequires:	python-pybtex
%endif # with doc

Requires:	python2-future
Requires:	numpy
Requires:	scipy
Requires:	python-lxml
%if 0%{?fedora}
Requires:	python2-matplotlib
%else
Requires:	python-matplotlib
%endif
Requires:	python-basemap
Requires:	python-sqlalchemy
Requires:	python-decorator
Requires:	python-requests
# These two are for matplotlib only
Requires:	python-mock
Requires:	python-nose

%global __provides_exclude_from ^%{python_sitearch}/.*\\.so$

%description
ObsPy is an open-source project dedicated to provide a Python framework for
processing seismological data. It provides parsers for common file formats,
clients to access data centers and seismological signal processing routines
which allow the manipulation of seismological time series (see Beyreuther
et al. 2010, Megies et al. 2011).

The goal of the ObsPy project is to facilitate rapid application development
for seismology.


%if %{with doc}
%package -n python-%{srcname}-doc
Summary:	A Python Toolbox for seismology/seismological observatories
Group:		Documentation

%description -n python-%{srcname}-doc
Documentation for ObsPy.
%endif # with doc


%prep
%autosetup -n %{srcname}-%{version} -p1

echo %{version} > obspy/RELEASE-VERSION

find -name '*.py' | xargs sed -i '1s|^#!python|#!%{__python2}|'


%build
CFLAGS="%{optflags}" %{__python2} setup.py --with-system-libmseed build
%{__python2} setup.py build_man


%install
%{__python2} setup.py install --skip-build --root %{buildroot}

pushd %{buildroot}%{_bindir}
for prog in %{obspy_programs};
do
	mv obspy-${prog} obspy-${prog}-%{python2_version}
	ln -s obspy-${prog}-%{python2_version} obspy-${prog}-2
%if 0%{?el7}
	ln -s obspy-${prog}-2 obspy-${prog}
%endif
done
popd

%{__python2} setup.py install_man --skip-build --root %{buildroot}

pushd %{buildroot}%{_mandir}/man1
for prog in %{obspy_programs};
do
	mv obspy-${prog}.1 obspy-${prog}-%{python2_version}.1
	ln -s obspy-${prog}-%{python2_version}.1 obspy-${prog}-2.1
%if 0%{?el7}
	ln -s obspy-${prog}-2.1 obspy-${prog}.1
%endif
done
popd

%if %{with doc}
pushd misc/docs
PYTHONPATH="%{buildroot}%{python2_sitearch}" make html PYTHON="%{__python2}"
rm build/html/.buildinfo
popd
%endif # with doc



%check
%if 0%{?fedora}
PYTHONPATH="%{buildroot}%{python2_sitearch}" PATH="%{buildroot}%{_bindir}:$PATH" \
	obspy-runtests-2 -d || exit 0
%else
PYTHONPATH="%{buildroot}%{python2_sitearch}" PATH="%{buildroot}%{_bindir}:$PATH" \
	OBSPY_NO_FLAKE8=yes obspy-runtests-2 -d || exit 0
%endif


%files
%doc README.md CHANGELOG.txt CONTRIBUTING.md
%license LICENSE.txt

%if 0%{?el7}
%{_bindir}/%{srcname}-dataless2resp
%{_bindir}/%{srcname}-dataless2xseed
%{_bindir}/%{srcname}-flinn-engdahl
%{_bindir}/%{srcname}-indexer
%{_bindir}/%{srcname}-mopad
%{_bindir}/%{srcname}-mseed-recordanalyzer
%{_bindir}/%{srcname}-plot
%{_bindir}/%{srcname}-print
%{_bindir}/%{srcname}-reftek-rescue
%{_bindir}/%{srcname}-runtests
%{_bindir}/%{srcname}-scan
%{_bindir}/%{srcname}-sds-report
%{_bindir}/%{srcname}-xseed2dataless
%endif

%{_bindir}/%{srcname}-dataless2resp-2
%{_bindir}/%{srcname}-dataless2xseed-2
%{_bindir}/%{srcname}-flinn-engdahl-2
%{_bindir}/%{srcname}-indexer-2
%{_bindir}/%{srcname}-mopad-2
%{_bindir}/%{srcname}-mseed-recordanalyzer-2
%{_bindir}/%{srcname}-plot-2
%{_bindir}/%{srcname}-print-2
%{_bindir}/%{srcname}-reftek-rescue-2
%{_bindir}/%{srcname}-runtests-2
%{_bindir}/%{srcname}-scan-2
%{_bindir}/%{srcname}-sds-report-2
%{_bindir}/%{srcname}-xseed2dataless-2

%{_bindir}/%{srcname}-dataless2resp-%{python2_version}
%{_bindir}/%{srcname}-dataless2xseed-%{python2_version}
%{_bindir}/%{srcname}-flinn-engdahl-%{python2_version}
%{_bindir}/%{srcname}-indexer-%{python2_version}
%{_bindir}/%{srcname}-mopad-%{python2_version}
%{_bindir}/%{srcname}-mseed-recordanalyzer-%{python2_version}
%{_bindir}/%{srcname}-plot-%{python2_version}
%{_bindir}/%{srcname}-print-%{python2_version}
%{_bindir}/%{srcname}-reftek-rescue-%{python2_version}
%{_bindir}/%{srcname}-runtests-%{python2_version}
%{_bindir}/%{srcname}-scan-%{python2_version}
%{_bindir}/%{srcname}-sds-report-%{python2_version}
%{_bindir}/%{srcname}-xseed2dataless-%{python2_version}

%if 0%{?el7}
%{_mandir}/man1/%{srcname}-dataless2resp.1*
%{_mandir}/man1/%{srcname}-dataless2xseed.1*
%{_mandir}/man1/%{srcname}-flinn-engdahl.1*
%{_mandir}/man1/%{srcname}-indexer.1*
%{_mandir}/man1/%{srcname}-mopad.1*
%{_mandir}/man1/%{srcname}-mseed-recordanalyzer.1*
%{_mandir}/man1/%{srcname}-plot.1*
%{_mandir}/man1/%{srcname}-print.1*
%{_mandir}/man1/%{srcname}-reftek-rescue.1*
%{_mandir}/man1/%{srcname}-runtests.1*
%{_mandir}/man1/%{srcname}-scan.1*
%{_mandir}/man1/%{srcname}-sds-report.1*
%{_mandir}/man1/%{srcname}-xseed2dataless.1*
%endif

%{_mandir}/man1/%{srcname}-dataless2resp-2.1*
%{_mandir}/man1/%{srcname}-dataless2xseed-2.1*
%{_mandir}/man1/%{srcname}-flinn-engdahl-2.1*
%{_mandir}/man1/%{srcname}-indexer-2.1*
%{_mandir}/man1/%{srcname}-mopad-2.1*
%{_mandir}/man1/%{srcname}-mseed-recordanalyzer-2.1*
%{_mandir}/man1/%{srcname}-plot-2.1*
%{_mandir}/man1/%{srcname}-print-2.1*
%{_mandir}/man1/%{srcname}-reftek-rescue-2.1*
%{_mandir}/man1/%{srcname}-runtests-2.1*
%{_mandir}/man1/%{srcname}-scan-2.1*
%{_mandir}/man1/%{srcname}-sds-report-2.1*
%{_mandir}/man1/%{srcname}-xseed2dataless-2.1*

%{_mandir}/man1/%{srcname}-dataless2resp-%{python2_version}.1*
%{_mandir}/man1/%{srcname}-dataless2xseed-%{python2_version}.1*
%{_mandir}/man1/%{srcname}-flinn-engdahl-%{python2_version}.1*
%{_mandir}/man1/%{srcname}-indexer-%{python2_version}.1*
%{_mandir}/man1/%{srcname}-mopad-%{python2_version}.1*
%{_mandir}/man1/%{srcname}-mseed-recordanalyzer-%{python2_version}.1*
%{_mandir}/man1/%{srcname}-plot-%{python2_version}.1*
%{_mandir}/man1/%{srcname}-print-%{python2_version}.1*
%{_mandir}/man1/%{srcname}-reftek-rescue-%{python2_version}.1*
%{_mandir}/man1/%{srcname}-runtests-%{python2_version}.1*
%{_mandir}/man1/%{srcname}-scan-%{python2_version}.1*
%{_mandir}/man1/%{srcname}-sds-report-%{python2_version}.1*
%{_mandir}/man1/%{srcname}-xseed2dataless-%{python2_version}.1*

%{python2_sitearch}/%{srcname}*


%if %{with doc}
%files -n python-%{srcname}-doc
%doc misc/docs/build/html
%endif # with doc


%changelog
* Thu May 02 2019 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.1.1-2
- Split apart Python 2 and 3 subpackages.
- Fix files in Python 2 subpackage.
- Remove now-unused patch.

* Wed May 01 2019 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.1.1-1
- Update to 1.1.1.
- Switch to tag archive URL.

* Wed May 01 2019 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.1.0-4
- Setup python2 subpackages. (quantum.analyst@gmail.com)
- Remove old conditions. (quantum.analyst@gmail.com)

* Sun Dec 09 2018 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.1.0-3
- Ignore test failures for now.

* Sat Nov 17 2018 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.1.0-2
- Backport patch to fix Sphinx 1.7.
- Add explicit gcc BR.

* Fri Oct 27 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.1.0-1
- Update to latest release.
- Stop unbundling evalresp.

* Fri Jul 14 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.3-2
- Patch in support for Matplotlib 2.

* Fri Mar 3 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.3-1
- New upstream release

* Wed Oct 19 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.2-3
- Fix compatibility with newer libmseed
- Patch several mseed issues

* Thu Sep 22 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.2-2
- Fix broken entry point

* Wed Aug 3 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.2-1
- New upstream release

* Sat Mar 26 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.1-2
- Remove gdal-python3 BR for Fedora 22, which doesn't support it
- Fix build against NumPy 1.11

* Sat Mar 26 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.1-1
- New upstream release

* Sun Feb 28 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.0-3
- Update future dependency to match official Fedora/EPEL naming

* Sat Feb 20 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.0-2
- Build documentation on Python 2 again

* Sat Feb 20 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.0.0-1
- New upstream release
- Make Python 3 the default binary if available
- Build documentation with Python 3 if available
- Add python-mock and python-nose to requirements since matplotlib falsely needs it
- Stop building documentation against mlpy
- Remove suds-jurko dependency

* Sat May 16 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.10.2-2
- Add BR on python-pybtex
- Patch to build with NumPy 1.4

* Fri May 15 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.10.2-1
- New upstream release

* Fri Mar 20 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.10.1-1
- New upstream release

* Thu Mar 19 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.10.0-2
- Fix error in stored version

* Thu Mar 19 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> 0.10.0-1
- Initial RPM release
