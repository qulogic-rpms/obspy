Name:		mars2mseed
Version:	1.4
Release:	2%{?dist}
License:	GPLv3+
Summary:	Convert MARS data to miniSEED format
Url:		https://github.com/iris-edu/mars2mseed
Source0:	https://github.com/iris-edu/%{name}/archive/v%{version}/%{name}-%{version}.tar.gz
Patch0001:	0001-Fix-strict-aliasing-warnings.patch

BuildRequires:	gcc
BuildRequires:	libmseed-devel >= 2.19.3

%description
Convert MARS 88/lite time series data to miniSEED.


%prep
%autosetup -n %{name}-%{version} -p1

# Remove bundled copy.
rm -rf libmseed


%build
pushd src
# Disable internal library copy by overriding CFLAGS and LDFLAGS
make %{?_smp_mflags} \
	CC=gcc CFLAGS="%{optflags}" \
	LDFLAGS='' \
	all
popd


%install
mkdir -p %{buildroot}%{_bindir}
cp -p mars2mseed %{buildroot}%{_bindir}

mkdir -p %{buildroot}%{_mandir}/man1
cp -p doc/*.1 %{buildroot}%{_mandir}/man1/


%files
%doc README.md ChangeLog
%license LICENSE
%{_bindir}/mars2mseed
%{_mandir}/man1/*


%changelog
* Sun Dec 09 2018 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.4-2
- Add explicit gcc BuildRequires.

* Thu Jun 08 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.4-1
- New upstream release

* Thu Feb 18 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.3-2
- Rebuild for Rawhide

* Thu Aug 21 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.3-1
- Initial package release
