Name:		ascii2mseed
Version:	1.5
Release:	2%{?dist}
License:	GPLv3+
Summary:	Convert simple ASCII data to miniSEED format
Url:		https://github.com/iris-edu/ascii2mseed
Source0:	https://github.com/iris-edu/%{name}/archive/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:	gcc
BuildRequires:	libmseed-devel >= 2.19.3

%description
The ascii2mseed program converts simple time series in ASCII format to
miniSEED.

Two input file types are supported: sample list (SLIST) and time-sample
pair (TSPAIR), see examples in documentation.


%prep
%autosetup -n %{name}-%{version}

# Remove bundled copy.
rm -rf libmseed


%build
pushd src
# Disable internal library copy by overriding CFLAGS and LDFLAGS
make %{?_smp_mflags} \
	CC=gcc CFLAGS="%{optflags}" \
	LDFLAGS='' \
	all
popd


%install
mkdir -p %{buildroot}%{_bindir}
cp -p ascii2mseed %{buildroot}%{_bindir}

mkdir -p %{buildroot}%{_mandir}/man1
cp -p doc/*.1 %{buildroot}%{_mandir}/man1/


%files
%doc README.md ChangeLog doc/SimpleASCIITimeseries.txt
%license LICENSE
%{_bindir}/ascii2mseed
%{_mandir}/man1/*


%changelog
* Sun Dec 09 2018 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.5-2
- Add explicit gcc BuildRequires.

* Wed Jun 7 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.5-1
- New upstream release

* Thu Feb 18 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.4-2
- Rebuild for Rawhide

* Thu May 28 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.4-1
- New upstream release

* Thu Mar 12 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.2-1
- New upstream release

* Thu Aug 21 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.1-1
- Initial package release
