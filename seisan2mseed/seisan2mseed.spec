Name:		seisan2mseed
Version:	1.8
Release:	2%{?dist}
License:	GPLv3+
Summary:	Convert SEISAN data to miniSEED format
Url:		https://github.com/iris-edu/seisan2mseed
Source0:	https://github.com/iris-edu/%{name}/archive/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:	gcc
BuildRequires:	libmseed-devel >= 2.19.3

%description
Convert SEISAN time series data to miniSEED format.


%prep
%autosetup -n %{name}-%{version}

# Remove bundled copy.
rm -rf libmseed


%build
pushd src
# Disable internal library copy by overriding REQCFLAGS and LDFLAGS
make %{?_smp_mflags} \
	CC=gcc CFLAGS="%{optflags}" \
	REQCFLAGS='' LDFLAGS='' \
	all
popd


%install
mkdir -p %{buildroot}%{_bindir}
cp -p seisan2mseed %{buildroot}%{_bindir}

mkdir -p %{buildroot}%{_mandir}/man1
cp -p doc/*.1 %{buildroot}%{_mandir}/man1/


%files
%doc README.md ChangeLog
%license LICENSE
%{_bindir}/seisan2mseed
%{_mandir}/man1/*


%changelog
* Sun Dec 09 2018 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.8-2
- Add explicit gcc BuildRequires.

* Sat Dec 08 2018 Elliott Sales de Andrade <quantum.analyst@gmail.com> 1.8-1
- New upstream release

* Thu Jun 08 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.7-1
- New upstream release

* Thu Feb 18 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.6-2
- Rebuild for Rawhide

* Thu Aug 21 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 1.6-1
- Initial package release
