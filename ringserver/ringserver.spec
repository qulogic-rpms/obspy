Name:		ringserver
Version:	2017.052
Release:	1%{?dist}
License:	GPLv2+
Summary:	A generic ring buffer and a SeedLink server
Url:		https://seiscode.iris.washington.edu/projects/ringserver
Source0:	https://github.com/iris-edu/%{name}/archive/v%{version}/%{name}-%{version}.tar.gz

BuildRequires:	mxml-devel >= 2.10
BuildRequires:	pcre-devel >= 8.39
BuildRequires:	libmseed-devel >= 2.18

%description
Ringserver is a generic ring buffer. The server is commonly used as a buffer
for (near) real-time streaming feeds of time series data. Each individual time
series is usually identified with a stream name (for seismological data these
are equivalent to unique channels). The stream names are completely arbitrary
allowing conventions to be created for specific needs.

In general, data clients connect to the server and submit a "subscription" for
specific streams and as other data clients submit data into the system they are
sent to clients with matching subscriptions. By identifying each buffer packet
uniquely and allowing a client to select a starting point in the buffer,
clients can resume broken connections without data loss.

Communicating with a server is generally done using the DataLink protocol over
TCP/IP. A DataLink client can query the server for various server details,
submit data, and subscribe to streams. Server details can include stream lists,
client lists and server statistics. A library in C that implements the DataLink
protocol is available.


%prep
%autosetup -n %{name}-%{version}

# Remove bundled copies.
rm -rf libmseed
rm -rf mxml
rm -rf pcre


%build
pushd src
# Disable internal library copy by overridding LDLIBS
make %{?_smp_mflags} \
	CC=gcc \
	CFLAGS="%{optflags}" \
	LDLIBS="-lpcre -lmseed -lmxml -lpthread" \
	all
popd


%install
mkdir -p %{buildroot}%{_bindir}
cp -p ringserver %{buildroot}%{_bindir}

mkdir -p %{buildroot}%{_mandir}/man1
cp -p doc/*.1 %{buildroot}%{_mandir}/man1/


%files
%doc README.md ChangeLog doc/HowTo-SeedLink-server.txt doc/ring.conf
%license LICENSE
%{_bindir}/ringserver
%{_mandir}/man1/*


%changelog
* Thu Jun 08 2017 Elliott Sales de Andrade <quantum.analyst@gmail.com> 2017.052-1
- New upstream release

* Thu Feb 18 2016 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2015.188-2
- Rebuild for Rawhide

* Tue Jul 7 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2015.188-1
- New upstream release

* Wed Jul 1 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2015.182-1
- New upstream release

* Fri Jun 5 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2015.156-1
- New upstream release

* Thu Mar 12 2015 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2014.269-1
- New upstream release

* Wed Aug 20 2014 Elliott Sales de Andrade <quantum.analyst@gmail.com> - 2013.284-1
- Initial package release
